class Settings {
  final String imageWatermark;
  final bool compressOriginalImage;
  final int compressionRatioImage;
  final int maxSizeOriginalImage;

  Settings({
    required this.imageWatermark,
    required this.compressOriginalImage,
    required this.compressionRatioImage,
    required this.maxSizeOriginalImage,
  });
}
