import 'dart:typed_data';

import 'package:image_picker/image_picker.dart';

class ImageAdd {
  String id;
  double percentLoading;
  final XFile imageOriginal;
  Uint8List? imageTransform;
  String? errorLoading;

  ImageAdd({
    required this.id,
    this.percentLoading = 0,
    required this.imageOriginal,
    this.imageTransform,
    this.errorLoading,
  });

  void setImageTransform(Uint8List newImage) {
    imageTransform = newImage;
  }

  void setPercentLoading(double newPercent) {
    percentLoading = newPercent;
  }
}
