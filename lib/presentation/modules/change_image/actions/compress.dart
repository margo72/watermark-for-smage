import 'dart:typed_data';

import 'package:flutter_image_compress/flutter_image_compress.dart';

abstract class ImageCompress {
  Future<Uint8List> compressWithList(Uint8List imgBytes, int inSampleSize);
}

class Compress implements ImageCompress {
  Future<Uint8List> compressImage(Uint8List imgBytes, double minImageKb,
      [int inSampleSize = 1]) async {
    Uint8List newImgBytes = await compressWithList(imgBytes, inSampleSize);
    double kb = newImgBytes.lengthInBytes / 1024;
    if (kb > minImageKb) {
      return compressImage(newImgBytes, inSampleSize + 1);
    } else {
      return newImgBytes;
    }
  }

  @override
  Future<Uint8List> compressWithList(
      Uint8List imgBytes, int inSampleSize) async {
    Uint8List newImgBytes = await FlutterImageCompress.compressWithList(
      imgBytes,
      minHeight: 800,
      minWidth: 400,
      quality: 90,
      inSampleSize: inSampleSize,
    );
    return newImgBytes;
  }
}
