import 'dart:typed_data';
import 'package:image/image.dart';
import 'package:image_watermark/image_watermark.dart';

abstract class AddImageWatermark {
  Future<Uint8List> addImageWatermark(Uint8List imgWatermark, Uint8List img);
}

class AddWatermark implements AddImageWatermark {
  @override
  Future<Uint8List> addImageWatermark(
      Uint8List imgWatermark, Uint8List img) async {
    var decodeImage1 = decodeImage(imgWatermark);
    var decodeImage2 = decodeImage(img);
    int _imgBytesWidth = decodeImage1!.width;
    int _imgBytesHeight = decodeImage1.height;

    int _imgBytes2Width = decodeImage2!.width;
    int _imgBytes2Height = decodeImage2.height;
    double _coefWidth = _imgBytesWidth / _imgBytes2Width;
    int imgHeight = (_imgBytes2Height * _coefWidth).toInt();
    int imgWidth = (_imgBytes2Width * _coefWidth).toInt();

    Uint8List watermarkedImgBytes = await ImageWatermark.addImageWatermark(
      originalImageBytes: imgWatermark,
      waterkmarkImageBytes: img,
      imgWidth: imgHeight /*высота водянного знака*/,
      imgHeight: imgWidth /*ширина водяного знака*/,
      dstY: _coefWidth < 1
          ? _imgBytesHeight - imgHeight
          : _imgBytesHeight - _imgBytes2Height,
      dstX: _coefWidth < 1 ? 0 : _imgBytesWidth - _imgBytes2Width,
    );
    return watermarkedImgBytes;
  }
}
