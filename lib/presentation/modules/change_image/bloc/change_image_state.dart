import 'package:watermark_for_image/data/models/image_add.dart';

abstract class ChangeImageState {
  final List<ImageAdd> listImagesAdd;

  ChangeImageState(this.listImagesAdd);
}

class InitialChangeImageState extends ChangeImageState {
  InitialChangeImageState(List<ImageAdd> listImagesAdd) : super(listImagesAdd);
}

class LoadingChangeImageState extends ChangeImageState {
  LoadingChangeImageState(List<ImageAdd> listImagesAdd) : super(listImagesAdd);
}

class ProcessLoadingChangeImageState extends ChangeImageState {
  ProcessLoadingChangeImageState(List<ImageAdd> listImagesAdd)
      : super(listImagesAdd);
}

class ErrorChangeImageState extends ChangeImageState {
  final String errorText;

  ErrorChangeImageState(
    List<ImageAdd> listImagesAdd, {
    required this.errorText,
  }) : super(listImagesAdd);
}
