abstract class ChangeImageEvent {}

class AddPhotosFromGalleryEvent extends ChangeImageEvent {}

class AddPhotosFromCameraEvent extends ChangeImageEvent {}
