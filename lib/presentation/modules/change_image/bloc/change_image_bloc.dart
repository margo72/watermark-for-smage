import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:watermark_for_image/data/models/image_add.dart';
import 'package:watermark_for_image/presentation/modules/change_image/bloc/change_image_event.dart';
import 'package:watermark_for_image/presentation/modules/change_image/bloc/change_image_state.dart';
import 'package:watermark_for_image/presentation/modules/change_image/repository/change_image_error.dart';
import 'package:watermark_for_image/presentation/modules/change_image/repository/change_image_repository.dart';

class ChangeImageBloc extends Bloc<ChangeImageEvent, ChangeImageState> {
  final ChangeImageRepository _changeImageRepository = ChangeImageRepository();

  ChangeImageBloc() : super(InitialChangeImageState([])) {
    on<AddPhotosFromGalleryEvent>((event, emit) async {
      emit(LoadingChangeImageState(state.listImagesAdd));

      try {
        //добавить из галереи
        List<ImageAdd> _listImages =
            await _changeImageRepository.addImagesFromGallery();
        state.listImagesAdd.addAll(_listImages);
        emit(
          InitialChangeImageState(state.listImagesAdd),
        );
        //сжатие изображения
        final stream = _changeImageRepository.compressImageList(_listImages);
        await for (final event in stream) {
          int key = state.listImagesAdd
              .indexWhere((element) => element.id == event.id);
          state.listImagesAdd[key] = event;
          emit(
            InitialChangeImageState(state.listImagesAdd),
          );
          //наложить водяной знак
          ImageAdd imageIdd = await _changeImageRepository
              .applyWatermark(state.listImagesAdd[key]);
          state.listImagesAdd[key] = imageIdd;
          emit(
            InitialChangeImageState(state.listImagesAdd),
          );
        }
      } on ErrorIsNullListImage catch (error) {
        emit(
          ErrorChangeImageState(
            state.listImagesAdd,
            errorText: error.toString(),
          ),
        );
      } catch (e) {
        emit(ErrorChangeImageState(state.listImagesAdd,
            errorText: 'Ошибка выполнения операции'));
      }
      debugPrint('Hello');
    });

    on<AddPhotosFromCameraEvent>((event, emit) async {
      emit(LoadingChangeImageState(state.listImagesAdd));

      try {
        //сделать живую фотографию и добавить ее
        ImageAdd images = await _changeImageRepository.addImagesFromCamera();
        state.listImagesAdd.add(images);
        emit(
          InitialChangeImageState(state.listImagesAdd),
        );
        //сжатие изображения
        images = await _changeImageRepository.compressImage(images);
        int key = state.listImagesAdd
            .indexWhere((element) => element.id == images.id);
        state.listImagesAdd[key] = images;
        emit(
          InitialChangeImageState(state.listImagesAdd),
        );
        //наложить водяной знак
        ImageAdd imageIdd = await _changeImageRepository
            .applyWatermark(state.listImagesAdd[key]);
        state.listImagesAdd[key] = imageIdd;
        emit(
          InitialChangeImageState(state.listImagesAdd),
        );
      } catch (e) {
        emit(
          ErrorChangeImageState(
            state.listImagesAdd,
            errorText: 'Ошибка выполнения операции',
          ),
        );
      }
    });
  }
}
