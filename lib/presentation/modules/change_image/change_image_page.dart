import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:watermark_for_image/data/models/image_add.dart';
import 'package:watermark_for_image/presentation/modules/change_image/bloc/change_image_bloc.dart';
import 'package:watermark_for_image/presentation/modules/change_image/bloc/change_image_state.dart';
import 'package:watermark_for_image/presentation/modules/change_image/widgets/way_add_photo_alert_dialog.dart';
import 'package:watermark_for_image/presentation/modules/photo_view/photo_view_page.dart';
import 'package:watermark_for_image/presentation/modules/settings/settings_page.dart';

class ChangeImagePage extends StatelessWidget {
  const ChangeImagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return ChangeImageBloc();
      },
      child: const ChangeImagePageView(),
    );
  }
}

class ChangeImagePageView extends StatelessWidget {
  const ChangeImagePageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Наложить водяной знак'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const SettingsPage(),
                ),
              );
            },
            icon: const Icon(Icons.settings),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Column(
          children: [
            Expanded(
              child: _AddWatermarkOnImage(),
            ),
            _DownloadNewImage(),
          ],
        ),
      ),
    );
  }
}

class _AddWatermarkOnImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChangeImageBloc, ChangeImageState>(
      builder: (context, state) => GridView.count(
        crossAxisCount: 2,
        padding: const EdgeInsets.all(8),
        children:
            List<Widget>.generate(state.listImagesAdd.length + 1, (index) {
          if (index < state.listImagesAdd.length) {
            return Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 2.0, vertical: 4.0),
              child: _Image(
                key: GlobalKey(debugLabel: state.listImagesAdd[index].id),
                imageAdd: state.listImagesAdd[index],
              ),
            );
          } else {
            return Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 2.0, vertical: 4.0),
              child: _AddImageButton(),
            );
          }
        }),
      ),
    );
  }
}

class _DownloadNewImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {},
      child: const Text(
        'Скачать все изображения',
        style: TextStyle(fontSize: 16.0),
      ),
    );
  }
}

class _AddImageButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      child: Center(
        child: IconButton(
          onPressed: () async {
            final result = await showDialog(
              context: context,
              builder: (BuildContext context) => const WayAddPhotoAlertDialog(),
            );
            if (result != null) context.read<ChangeImageBloc>().add(result);
            //context.read().add(result);
          },
          icon: const Icon(
            Icons.add,
            color: Colors.grey,
            size: 40.0,
          ),
        ),
      ),
    );
  }
}

class _Image extends StatefulWidget {
  final ImageAdd imageAdd;

  const _Image({
    Key? key,
    required this.imageAdd,
  }) : super(key: key);

  @override
  State<_Image> createState() => _ImageState();
}

class _ImageState extends State<_Image> {
  late double value;
  late double maxValue;

  @override
  void initState() {
    value = widget.imageAdd.percentLoading / 100;
    maxValue = (widget.imageAdd.percentLoading + 30) / 100;
    downloadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: widget.imageAdd.percentLoading == 100
          ? Stack(
              children: [
                Hero(
                  tag: 'image',
                  child: Image.asset(
                    'images/main.jpg',
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.cover,
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width / 8,
                    height: MediaQuery.of(context).size.width / 8,
                    child: IconButton(
                      icon: const Icon(
                        Icons.cancel,
                        color: Colors.black54,
                        size: 30.0,
                      ),
                      onPressed: () {},
                    ),
                  ),
                ),
              ],
            )
          : Stack(
              children: [
                Container(
                  child: widget.imageAdd.imageTransform == null
                      ? Container()
                      : Hero(
                          tag: 'image',
                          child: Image.memory(
                            widget.imageAdd.imageTransform!,
                            width: MediaQuery.of(context).size.width / 2,
                            height: MediaQuery.of(context).size.width / 2,
                            fit: BoxFit.cover,
                          ),
                        ),
                ),
                Container(
                  color: const Color.fromRGBO(38, 38, 38, 0.6),
                ),
                Center(
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.grey,
                    color: Colors.blue,
                    strokeWidth: 5,
                    value: value,
                  ),
                ),
              ],
            ),
      onDoubleTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const PhotoViewPage(path: 'images/main.jpg'),
          ),
        );
      },
    );
  }

  void downloadData() {
    Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      setState(() {
        if (maxValue == 1) {
          timer.cancel();
        } else {
          value = value + 0.01;
        }
      });
    });
  }
}
