import 'dart:io';
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';
import 'package:watermark_for_image/data/models/image_add.dart';
import 'package:watermark_for_image/data/models/repository.dart';
import 'package:watermark_for_image/presentation/modules/change_image/actions/add_watermark.dart';
import 'package:watermark_for_image/presentation/modules/change_image/actions/compress.dart';
import 'package:watermark_for_image/presentation/modules/change_image/repository/change_image_error.dart';
import 'package:watermark_for_image/presentation/modules/change_image/actions/pick_image.dart';

final repository = Repository();

abstract class AddImageRepository {
  Future<List<ImageAdd>> addImagesFromGallery();

  Future<ImageAdd> addImagesFromCamera();
}

abstract class CompressImageRepository {
  Stream<ImageAdd> compressImageList(List<ImageAdd> listImageAdd);

  Future<ImageAdd> compressImage(ImageAdd imageAdd, [int inSampleSize]);

  Future<Uint8List> fileToUint8List(File file);
}

abstract class AddWatermarkRepository {
  Future<ImageAdd> applyWatermark(ImageAdd oldImageAdd);
}

class ChangeImageRepository
    implements
        AddImageRepository,
        CompressImageRepository,
        AddWatermarkRepository {
  @override
  Future<List<ImageAdd>> addImagesFromGallery() async {
    List<ImageAdd> _listImageAdd = [];

    final List<XFile>? images = await PickImages().pickMultiImage();

    if (images == null) {
      throw ErrorIsNullListImage();
    }
    for (var element in images) {
      String id = const Uuid().v4();
      _listImageAdd.add(
        ImageAdd(
          id: id,
          percentLoading: 30,
          imageOriginal: element,
        ),
      );
    }
    return _listImageAdd;
  }

  @override
  Future<ImageAdd> addImagesFromCamera() async {
    final XFile? image = await PickImages().pickImage();

    if (image == null) {
      throw ErrorIsNullListImage();
    }
    String id = const Uuid().v4();
    return ImageAdd(
      id: id,
      percentLoading: 30,
      imageOriginal: image,
    );
  }

  @override
  Stream<ImageAdd> compressImageList(List<ImageAdd> listImageAdd) async* {
    for (var imageAdd in listImageAdd) {
      ImageAdd updateImageAdd = await compressImage(imageAdd);
      yield updateImageAdd;
    }
  }

  @override
  Future<Uint8List> fileToUint8List(File file) async {
    var _t = await file.readAsBytes();
    return Uint8List.fromList(_t);
  }

  @override
  Future<ImageAdd> compressImage(ImageAdd imageAdd,
      [int inSampleSize = 1]) async {
    final bool compressOriginalImage =
        await repository.getCompressOriginalImage();
    if (compressOriginalImage == false) {
      return imageAdd;
    } else {
      Uint8List imgBytes =
          await fileToUint8List(File(imageAdd.imageOriginal.path));
      double minImageKb = 100.0;
      Uint8List newImgBytes =
          await Compress().compressImage(imgBytes, minImageKb);
      imageAdd.imageTransform = newImgBytes;
      imageAdd.percentLoading = 70;
      return imageAdd;
    }
  }

  @override
  Future<ImageAdd> applyWatermark(ImageAdd oldImageAdd) async {
    //превратим картинку водяного знака в нужное изображенеи
    final ByteData bytes = await rootBundle.load('images/watermark.png');
    Uint8List imgWatermark = bytes.buffer.asUint8List();

    Uint8List? img = oldImageAdd.imageTransform;

    if (img == null) {
      oldImageAdd.percentLoading = 100;
      oldImageAdd.errorLoading = 'Изображение было потерянно';
      return oldImageAdd;
    }

    Uint8List watermarkedImgBytes =
        await AddWatermark().addImageWatermark(imgWatermark, img);

    oldImageAdd.percentLoading = 100;
    oldImageAdd.imageTransform = watermarkedImgBytes;
    return oldImageAdd;
  }
}
