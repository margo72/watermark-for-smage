import 'package:flutter/material.dart';

class DropDownList extends StatefulWidget {
  final List<String> list;
  final int compress;
  final String signature;
  final void Function(int)? onPressed;

  const DropDownList({
    Key? key,
    required this.list,
    required this.compress,
    required this.signature,
    this.onPressed,
  }) : super(key: key);

  @override
  State<DropDownList> createState() => _DropDownListState();
}

class _DropDownListState extends State<DropDownList> {
  int selectedIndex = 0;
  bool isShowList = false;

  @override
  void initState() {
    widget.list.asMap().forEach((key, value) {
      if (value == widget.compress.toString()) {
        selectedIndex = key;
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              isShowList = !isShowList;
            });
          },
          child: Container(
              alignment: Alignment.bottomLeft,
              padding:
                  const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2.0),
              margin: const EdgeInsets.only(bottom: 2.0),
              color: Colors.grey[200],
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                    child: Text(
                      widget.list[selectedIndex] + widget.signature,
                      style: const TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                  Icon(
                    isShowList ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                  ),
                ],
              )),
        ),
        isShowList
            ? Column(
                children: List<Widget>.generate(
                  widget.list.length,
                  (index) => GestureDetector(
                    onTap: () {
                      setState(
                        () {
                          selectedIndex = index;
                          isShowList = false;
                        },
                      );
                      if (widget.onPressed != null) {
                        widget.onPressed!(index);
                      }
                    },
                    child: Container(
                      alignment: Alignment.bottomLeft,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 2.0),
                      margin: const EdgeInsets.only(bottom: 2.0),
                      color: Colors.grey[200],
                      child: Text(
                        widget.list[index] + widget.signature,
                        style: const TextStyle(fontSize: 16.0),
                      ),
                    ),
                  ),
                ),
              )
            : const SizedBox(),
      ],
    );
  }
}
